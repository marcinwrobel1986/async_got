import json
import aiohttp

from sanic import Sanic
from sanic_jinja2 import SanicJinja2


class DataCollection:
    def __init__(self):
        self.houses_endpoint = 'https://www.anapioficeandfire.com/api/houses'

    @staticmethod
    async def get_data(url: str):
        async with aiohttp.ClientSession() as session:
            async with session.get(url) as resp:
                response = await resp.text()
                body_json = json.loads(response)

                return body_json

    @staticmethod
    def get_houses(data):
        houses_details = [(house.get('name', ''), house.get('swornMembers', '')) for house in data]

        return houses_details

    @staticmethod
    def get_sworn_member(data, house):
        end_point = [record[1] for record in data if house == record[0]][0]

        return end_point

    @staticmethod
    def get_sworn_member_name(data):
        members_name = data.get('name', '')

        return members_name


app = Sanic()
jinja = SanicJinja2(app)
dc = DataCollection()


@app.route('/')
@jinja.template('houses.html')
async def houses(request):
    houses_data = await dc.get_data(dc.houses_endpoint)
    dc.houses = dc.get_houses(houses_data)

    return {'houses': dc.houses}


@app.route('/<house>')
@jinja.template('houses.html')
async def sworn_member(request, house):

    class SwornMembersHelper:
        def __init__(self, house):
            self.house = house

        async def __aenter__(self):
            return self

        async def __aexit__(self, *_):
            pass

        async def __aiter__(self):
            sworn_members = dc.get_sworn_member(dc.houses, house)
            for member in sworn_members:
                yield member

    sworn_members_names = []

    async with SwornMembersHelper(house) as smh:
        async for member in smh:
            members_data = await dc.get_data(member)
            members_name = dc.get_sworn_member_name(members_data)
            sworn_members_names.append(members_name)

    return {'houses': dc.houses, 'sworn_members_names': sworn_members_names}


if __name__ == '__main__':
    app.run(host='127.0.0.1', port=8000, debug=False)
